# Vežbe 8

**Cilj vežbi:** Implementirati Spring Boot aplikaciju sa proverama prava pristupa.

1. Proširiti prethodni primer upotrebom Spring Boot secrurity zavisnosti. Za svaku od ruta ograničiti prava prustipa. Operacije čitanja omogućiti običnim korisnicima a operacije dodavanja, brisanja i izmene administratorima. Neprijavljenim korisnicima zabraniti pristup.
___
### Dodatne napomene:
* Dokumentacij za Spring Data JPA: https://docs.spring.io/spring-data/jpa/docs/current/reference/html.
* Dokumentacija za Spring security: https://docs.spring.io/spring-security/reference/index.html
___
