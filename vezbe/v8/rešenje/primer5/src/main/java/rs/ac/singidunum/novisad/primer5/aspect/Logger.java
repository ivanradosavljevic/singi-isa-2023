package rs.ac.singidunum.novisad.primer5.aspect;

import java.util.Date;
import java.util.Optional;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import rs.ac.singidunum.novisad.primer5.model.Korisnik;
import rs.ac.singidunum.novisad.primer5.model.Log;
import rs.ac.singidunum.novisad.primer5.service.LogService;

@Component
@Aspect
public class Logger {
	
	@Autowired
	LogService logService;

	@Before("@annotation(LoggedMethod)")
	public void log(JoinPoint jp) {
		logService.save(new Log(null,new Date(), jp.getSignature().toString()));
//		System.out.println(jp.getSignature().toString());
//		System.out.println("Logovanje podataka");
	}
	
//	@Before("execution(* rs.ac.singidunum.novisad.primer5.service.*.*())")
//	public void log(JoinPoint jp) {
//		logService.save(new Log(null,new Date(), jp.getSignature().toString()));
////		System.out.println(jp.getSignature().toString());
////		System.out.println("Logovanje podataka");
//	}
	
	@Before("execution(* rs.ac.singidunum.novisad.primer5.service.*.*(..)) && args(id, ..)")
	public void log(JoinPoint jp, Long id) {
		System.out.println(jp.toString());
		System.out.println("Logovanje podataka " + id);
	}
	
	@AfterReturning(pointcut ="execution(* rs.ac.singidunum.novisad.primer5.service.*.*(..)) && args(id, ..)", returning="retVal")
	public void logAfter(JoinPoint jp, Long id, Object retVal) {
		System.out.println(jp.toString());
		System.out.println("Logovanje podataka " + id);
		System.out.println(retVal);
	}
	
	@Around("execution(* rs.ac.singidunum.novisad.primer5.service.*.*(..)) && args(id, ..)")
	public Object logAround(ProceedingJoinPoint jp, Long id) throws Throwable {
		System.out.println("Around!");
		System.out.println(jp.toString());
		System.out.println("Logovanje podataka " + id);
//		jp.getArgs().
//		System.out.println(retVal);
		Object [] argumenti = new Object[1];
		argumenti[0] = 2l;
		Object retVal = jp.proceed(argumenti);
		
		Korisnik rezultat = ((Optional<Korisnik>)retVal).orElse(null);
		rezultat.setIme("TEST KORISNIK");
		return Optional.of(rezultat);
		
//		return rezultat;
//		return jp.proceed();
	}
}
