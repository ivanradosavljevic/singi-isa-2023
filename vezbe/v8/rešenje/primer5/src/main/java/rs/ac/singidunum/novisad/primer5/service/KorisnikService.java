package rs.ac.singidunum.novisad.primer5.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.primer5.aspect.LoggedMethod;
import rs.ac.singidunum.novisad.primer5.model.Korisnik;
import rs.ac.singidunum.novisad.primer5.repository.KorisnikRepository;

@Service
public class KorisnikService {
	@Autowired
	private KorisnikRepository korisnikRepository;
	
	@LoggedMethod
	public Iterable<Korisnik> findAll() {
		return korisnikRepository.findAll();
	}
	
	public Optional<Korisnik> findById(Long id) {
		return korisnikRepository.findById(id);
	}
}
