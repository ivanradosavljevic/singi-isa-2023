package rs.ac.singidunum.novisad.primer5.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.novisad.primer5.model.AdministrativniKorisnik;

@Repository
public interface AdministrativniKorisnikRepository extends CrudRepository<AdministrativniKorisnik, Long> {
	Optional<AdministrativniKorisnik> findByKorisnickoIme(String korisnickoIme);
}
