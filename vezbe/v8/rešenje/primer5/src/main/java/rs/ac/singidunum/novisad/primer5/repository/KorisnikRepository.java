package rs.ac.singidunum.novisad.primer5.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.novisad.primer5.model.Korisnik;

@Repository
public interface KorisnikRepository extends CrudRepository<Korisnik, Long> {
}
