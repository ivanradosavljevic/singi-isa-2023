package rs.ac.singidunum.novisad.primer5.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.primer5.model.Log;
import rs.ac.singidunum.novisad.primer5.repository.LogRepository;

@Service
public class LogService {
	@Autowired
	LogRepository lr;
	
	public void save(Log l) {
		lr.save(l);
	}
}
