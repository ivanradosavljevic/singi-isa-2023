package rs.ac.singidunum.novisad.primer5.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

@Entity
public class PravoPristupa {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String naziv;
	@ManyToOne()
	private AdministrativniKorisnik vlasnik;
	
	
	public PravoPristupa(Long id, String naziv, AdministrativniKorisnik vlasnik) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.vlasnik = vlasnik;
	}
	public PravoPristupa() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public AdministrativniKorisnik getVlasnik() {
		return vlasnik;
	}
	public void setVlasnik(AdministrativniKorisnik vlasnik) {
		this.vlasnik = vlasnik;
	}
	
	
}
