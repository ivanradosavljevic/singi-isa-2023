package rs.ac.singidunum.novisad.primer5.model;

import java.time.LocalDateTime;

import jakarta.persistence.Entity;

@Entity
public class KlijentskiKorisnik extends Korisnik {
	private LocalDateTime datumRegistracije;
	private String status;
	public KlijentskiKorisnik(LocalDateTime datumRegistracije, String status) {
		super();
		this.datumRegistracije = datumRegistracije;
		this.status = status;
	}
	public KlijentskiKorisnik() {
		super();
	}
	public KlijentskiKorisnik(String ime, String prezime, String email, String korisnickoIme, String lozinka) {
		super(ime, prezime, email, korisnickoIme, lozinka);
	}
	public KlijentskiKorisnik(String ime, String prezime, String email, String korisnickoIme, String lozinka,
			LocalDateTime datumRegistracije, String status) {
		super(ime, prezime, email, korisnickoIme, lozinka);
		this.datumRegistracije = datumRegistracije;
		this.status = status;
	}
	public LocalDateTime getDatumRegistracije() {
		return datumRegistracije;
	}
	public void setDatumRegistracije(LocalDateTime datumRegistracije) {
		this.datumRegistracije = datumRegistracije;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
