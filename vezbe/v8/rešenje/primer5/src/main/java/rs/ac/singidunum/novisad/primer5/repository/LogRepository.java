package rs.ac.singidunum.novisad.primer5.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.novisad.primer5.model.Log;

@Repository
public interface LogRepository extends CrudRepository<Log, Long> {

}
