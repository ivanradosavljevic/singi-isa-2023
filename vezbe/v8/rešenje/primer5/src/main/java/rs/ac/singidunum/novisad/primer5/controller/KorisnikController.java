package rs.ac.singidunum.novisad.primer5.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.novisad.primer5.aspect.LoggedMethod;
import rs.ac.singidunum.novisad.primer5.model.AdministrativniKorisnik;
import rs.ac.singidunum.novisad.primer5.model.KlijentskiKorisnik;
import rs.ac.singidunum.novisad.primer5.model.Korisnik;
import rs.ac.singidunum.novisad.primer5.service.AdministrativniKorisniciService;
import rs.ac.singidunum.novisad.primer5.service.KlijentskiKorisnikService;
import rs.ac.singidunum.novisad.primer5.service.KorisnikService;
import rs.ac.singidunum.novisad.primer5.utils.TokenUtils;

@Controller
@RequestMapping("/api")
public class KorisnikController {
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	TokenUtils tokenUtils;
	
	@Autowired
	UserDetailsService userDetailsService;

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	private KorisnikService korisnikService;

	@Autowired
	private KlijentskiKorisnikService klijentskiKorisnikService;

	@Autowired
	private AdministrativniKorisniciService administrativniKorisniciService;

	@RequestMapping("/korisnici")
	@LoggedMethod
	public ResponseEntity<Iterable<Korisnik>> findAllKorisnici() {
		System.out.println(korisnikService.findById(1l).get().getIme());
		return new ResponseEntity<Iterable<Korisnik>>(korisnikService.findAll(), HttpStatus.OK);
	}

	@Secured({"ROLE_USER"})
	@RequestMapping("/klijentskiKorisnici")
	public ResponseEntity<Iterable<KlijentskiKorisnik>> findAllKlijentskiKorisnici() {
		return new ResponseEntity<Iterable<KlijentskiKorisnik>>(klijentskiKorisnikService.findAll(), HttpStatus.OK);
	}

	@Secured({"ROLE_ADMIN"})
	@RequestMapping("/administrativniKorisnici")
	public ResponseEntity<Iterable<AdministrativniKorisnik>> findAllAdministrativniKorisnici() {
		return new ResponseEntity<Iterable<AdministrativniKorisnik>>(administrativniKorisniciService.findAll(),
				HttpStatus.OK);
	}

	@RequestMapping(path = "/login", method = RequestMethod.POST)
	public ResponseEntity<String> login(@RequestBody Korisnik korisnik) {
		System.out.println(userDetailsService.loadUserByUsername(korisnik.getKorisnickoIme()));
		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(korisnik.getKorisnickoIme(),
				korisnik.getLozinka());
		Authentication auth = authenticationManager.authenticate(token);
		SecurityContextHolder.getContext().setAuthentication(auth);
		
		String jwt = tokenUtils.generateToken(userDetailsService.loadUserByUsername(korisnik.getKorisnickoIme()));
		System.out.println(jwt);
		return new ResponseEntity<String>(jwt, HttpStatus.OK);
	}
}
