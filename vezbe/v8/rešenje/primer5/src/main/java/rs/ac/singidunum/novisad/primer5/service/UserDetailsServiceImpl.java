package rs.ac.singidunum.novisad.primer5.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;
import rs.ac.singidunum.novisad.primer5.model.AdministrativniKorisnik;
import rs.ac.singidunum.novisad.primer5.model.PravoPristupa;

@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	AdministrativniKorisniciService administrativniKorisniciService;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<AdministrativniKorisnik> korisnik = administrativniKorisniciService.findByKorisnickoIme(username);
		if(korisnik.isPresent()) {
			ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<>();
			for(PravoPristupa pravoPristupa : korisnik.get().getPravaPristupa()) {
				grantedAuthorities.add(new SimpleGrantedAuthority(pravoPristupa.getNaziv()));
			}
			return new User(korisnik.get().getKorisnickoIme(), korisnik.get().getLozinka(), grantedAuthorities);
		}
		return null;
	}

}
