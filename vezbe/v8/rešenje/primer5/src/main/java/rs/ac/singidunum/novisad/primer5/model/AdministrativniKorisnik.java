package rs.ac.singidunum.novisad.primer5.model;

import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;

@Entity
public class AdministrativniKorisnik extends Korisnik {
	@OneToMany(mappedBy = "vlasnik")
	private Set<PravoPristupa> pravaPristupa = new HashSet<>();

	public AdministrativniKorisnik() {
		super();
	}

	public AdministrativniKorisnik(String ime, String prezime, String email, String korisnickoIme, String lozinka) {
		super(ime, prezime, email, korisnickoIme, lozinka);
	}

	public Set<PravoPristupa> getPravaPristupa() {
		return pravaPristupa;
	}

	public void setPravaPristupa(Set<PravoPristupa> pravaPristupa) {
		this.pravaPristupa = pravaPristupa;
	}

}
