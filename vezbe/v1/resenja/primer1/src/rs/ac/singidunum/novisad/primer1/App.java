package rs.ac.singidunum.novisad.primer1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class App {

	public static void main(String[] args) throws InterruptedException {
		try (ServerSocket serverSocket = new ServerSocket(3000)) {
			while (true) {
				Socket socket = serverSocket.accept();
				
				PrintWriter output = new PrintWriter(socket.getOutputStream());
				BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				System.out.println(input.readLine());
				
				Thread.sleep(5000);
				
				output.println("odgovor");
				output.flush();
				
				input.close();
				output.close();
				socket.close();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
