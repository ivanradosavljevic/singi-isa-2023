package rs.ac.singidunum.novisad.primer1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ThreadedServer {

	public static void main(String[] args) throws InterruptedException {
		try (ServerSocket serverSocket = new ServerSocket(3000)) {
			while (true) {
				Socket socket = serverSocket.accept();
				RequestHandler requestHandler = new RequestHandler(socket);
				
				Thread thread = new Thread(requestHandler);
				thread.start();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
