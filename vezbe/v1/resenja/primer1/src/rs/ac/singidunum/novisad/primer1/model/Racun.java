package rs.ac.singidunum.novisad.primer1.model;

import java.util.ArrayList;

public class Racun {
	private Korisnik vlasnik;
	private String brojRacuna;
	private double stanje;
	private ArrayList<Transakcija> transakcije = new ArrayList<>();

	public Racun() {
		super();
	}

	public Racun(Korisnik vlasnik, String brojRacuna, double stanje) {
		super();
		this.vlasnik = vlasnik;
		this.brojRacuna = brojRacuna;
		this.stanje = stanje;
	}

	public Korisnik getVlasnik() {
		return vlasnik;
	}

	public void setVlasnik(Korisnik vlasnik) {
		this.vlasnik = vlasnik;
	}

	public String getBrojRacuna() {
		return brojRacuna;
	}

	public void setBrojRacuna(String brojRacuna) {
		this.brojRacuna = brojRacuna;
	}

	public double getStanje() {
		return stanje;
	}

	public void setStanje(double stanje) {
		this.stanje = stanje;
	}

	public ArrayList<Transakcija> getTransakcije() {
		return transakcije;
	}

	public void setTransakcije(ArrayList<Transakcija> transakcije) {
		this.transakcije = transakcije;
	}
}
