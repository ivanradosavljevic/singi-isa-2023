# Vežbe 2

**Cilj vežbi:** Implementacija poslovne aplikacije uz pomoć Spring radnog okvira.

1. Kreirati novi Maven projekat i uključiti zavisnosti za jezgro Spring radnog okvira, iskonfigurisati plugin za pakovanje kompajliranih klasa u jar datoteke.
2. Model sa prethodnih vežbi dodati u novonapravljenu aplikaciju.
3. Napisati klasu koja predstavlja repozitorijum računa, preko repozitorijuma je moguće dobaviti sve račune, dobaviti jedan račun po identifikatoru, dobaviti račune po opsegu stanja, uskladištiti nove račune, izmeniti i obrisati postojeće račune.
4. Napisati klasu u kojoj se implementira poslovna logika za račune. Račun je moguće, pregledati, dodavati, uklanjati, izmeniti i pretraživati po stanju. Pored ovih operacija račun je moguće blokirati, prilikom blokiranja sva sredstva sa računa se uklanjaju i račun postaje nedostupan korisniku za upotrebu.
5. Napisati klasu za prikaz računa i pristup metodama poslovne logike kroz konzolni korisnički interfejs.
6. Napraviti Spring aplikaciju koja koristi prethodno navedene klase za realizaciju aplikacije za upravljanje računima.
___
### Dodatne napomene:
* Eclipse EE se može naći na adresi: https://www.eclipse.org/downloads/packages/.
* Dokumentacija za jezgro Spring radnog okvira: https://docs.spring.io/spring/docs/current/spring-framework-reference/core.html#spring-core.
___
