# Vežbe 7

**Cilj vežbi:** Implementirati Spring Boot aplikaciju za upravljanje različitim tipovima korisnika.

1. Napistati model za korisnike, administrativne korisnike i klijentske korisnike. Svi korisnici su definisani jedinstvenim korisničkim imenom, email adresom, lozinkom, imenom i prezimenom. Administrativni korisnici su korisnici koji imaju spisak administrativnih privilegija. Klijentski korisnici su korisnici koji imaju datum registracije, status i spisak klijentskih privilegija.
2. Namapirati modele na tabele u relacionoj bazi podataka.
3. Napraviti repozitorijume za pristup podacima.
4. Napraviti servise i kontrolere za sve entitete.
5. Omogućiti implicitno vođenje evidencije o promenama prilikom rada sa korisnicima. Svaku izmenu zabeležiti u bazi podataka. Izmena se beleži kao objekat koji sadrži datum i vreme izmene i slobodan tekst kao opis izmene.
___
### Dodatne napomene:
* Dokumentacij za Spring Data JPA: https://docs.spring.io/spring-data/jpa/docs/current/reference/html.
* Dokumentacija za AOP u Spring radnom okviru: https://docs.spring.io/spring-framework/docs/current/reference/html/core.html#aop
___
